[0.1.0]
* Initial version

[0.2.0]
* Fix symlink

[0.3.0]
* Set `enableregistration` only on first run

[0.4.0]
* Update logo

[0.4.1]
* Update to vikunja 0.16.1

[0.5.0]
* Update to Vikunja 0.17.0

[0.6.0]
* Use info endpoint of the api to healthcheck the api and not the local webserver
* Rename binary to vikunja as this aligns with the official documentation
* link binary to /usr/local/bin to make it easier to run via the shell
* Refactor version variables in Dockerfile
* Fix url in mail notifications
* Thanks felix

[0.7.0]
* Update to vikunja 0.18.0
* Remove duplicate license file
* [Release blog](https://vikunja.io/blog/2021/09/whats-new-in-vikunja-0.18.0/)
* Thanks felix

[1.0.0]
* Make it stable with vikunja 0.18.0

[1.0.1]

## Backend 0.18.1

### Fixed

* Docs: Add another third-party tutorial link
* Don't try to export items which do not have a parent
* fix(deps): update golang.org/x/sys commit hash to 6f6e228 (#970)
* fix(deps): update golang.org/x/sys commit hash to c212e73 (#971)
* Fix exporting tasks from archived lists
* Fix lint
* Fix tasks not exported
* Fix tmp export file created in the wrong path

## Frontend 0.18.1

### Added

* feat: make it possible to fake online state via dev env (#720)

### Fixed

* fix: call to /null from background image (#714)
* Fix data export download progress
* fix: kanban-card mutatation violation (#712)
* Fix missing translation when creating a new task on the kanban board
* Fix rearranging tasks in a kanban bucket when its limit was reached
* Fix sort order for table view
* Fix task attributes overridden when saving the task title with enter
* Fix translation badge

### Dependency Updates

* Update dependency @4tw/cypress-drag-drop to v2 (#711)
* Update dependency axios to v0.21.4 (#705)
* Update dependency jest to v27.1.1 (#716)
* Update dependency vite-plugin-vue2 to v1.8.2 (#707)
* Update dependency vite to v2.5.4 (#708)
* Update dependency vite to v2.5.5 (#709)
* Update typescript-eslint monorepo to v4.31.0 (#706)

[1.0.2]
* Update Vikunja frontend to 0.18.2
* Update to base image 3.2.0
* fix: edit saved filter title

[1.0.3]
* Fix large file uploads

[1.1.0]
* Update Vikunja to 0.19.0

[1.1.1]
* Update Vikunja to 0.19.1

[1.1.2]
* Update Vikunja API to 0.19.2

[1.1.3]
* Fix email sending

[1.2.0]
* Update Vikunja API to 0.20.0
* Update Vikunja frontend to 0.20.0

[1.2.1]
* Update Vikunja API to 0.20.1
* Update Vikunja frontend to 0.20.1

[1.2.2]
* Update Vikunja frontend to 0.20.2
* Update Cloudron base image to 4.0.0

[1.2.3]
* Update Vikunja API to 0.20.2
* Update Vikunja frontend to 0.20.3

