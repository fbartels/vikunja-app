## About

Vikunja is an Open-Source, self-hosted To-Do list application for all platforms. It is licensed under the GPLv3.

Think of Vikunja like the notebook you have with all your things to keep track of.
But better.

## Features

* Stay organized - Organize your tasks in lists, organize lists in namespaces.
* Tasks - Tasks are not only simple tasks. You can let Vikunja remind you of tasks when they're due. Never miss an important deadline again!
* Collaboration - Ever wished you could just share that grocery list with your roomate instead of having to send dozens of texts on your way to the supermarket? With Vikunja, you can. Simply share a list (or a namespace with all its lists) to another user. Don't want your roommate to add new things to the grocery list and only do the shopping? You can also share a list with read-only access!
*  Share links - You can share a list with a link so that others can directly see or edit all tasks on a list but don't need to create an account. Share links have all the same rights management as sharing with users or teams. 
* Labels - Effortlessly mark tasks with a colorful label to find and group relevant tasks with a click! 

