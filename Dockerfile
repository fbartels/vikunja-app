FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

# Create code folder
RUN mkdir -p /app/code/api /app/code/frontend
WORKDIR /app/code

# Those ARGs need to be adapted in order to setup a specific version.
# https://github.com/go-vikunja/api/tags
ARG VERSIONAPI=0.20.2
# https://github.com/go-vikunja/frontend/tags
ARG VERSIONFRONTEND=0.20.3

### Vikunja-API
# Download and unzip vikunja api
RUN wget https://dl.vikunja.io/api/${VERSIONAPI}/vikunja-v${VERSIONAPI}-linux-amd64-full.zip -O /tmp/vikunja-api.zip && \
    unzip /tmp/vikunja-api.zip -d /app/code/api && \
    ln -s /app/code/api/vikunja-v${VERSIONAPI}-linux-amd64 /app/code/api/vikunja && \
    ln -s /app/code/api/vikunja-v${VERSIONAPI}-linux-amd64 /usr/local/bin/vikunja

# Create a symbolic link so config is where the app expects it
RUN ln -s /app/data/config.yml /app/code/api/config.yml

### Vikunja Frontend
# Download and unzip vikunja frontend
RUN wget https://dl.vikunja.io/frontend/vikunja-frontend-${VERSIONFRONTEND}.zip -O /tmp/vikunja-frontend-${VERSIONFRONTEND}.zip && \
    unzip /tmp/vikunja-frontend-${VERSIONFRONTEND}.zip -d /app/code/frontend

# add nginx config
USER root
RUN rm /etc/nginx/sites-enabled/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
ADD vikunja-nginx /etc/nginx/sites-enabled/vikunja

ADD nginx_readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf

ADD supervisor/ /etc/supervisor/conf.d/

# setup log paths
RUN sed -e 's,^logfile=.*$,logfile=/run/vikunja/supervisord.log,' -i /etc/supervisor/supervisord.conf

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
