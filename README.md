# Vikunja Cloudron App

This repository contains the Cloudron app package source for [Vikunja](http://www.vikunja.io/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=io.vikunja.cloudronapp)

or using the [Cloudron command line tooling](https://git.cloudron.io/cloudron/cloudron-cli/)

```
cloudron install --appstore-id io.vikunja.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://git.cloudron.io/cloudron/cloudron-cli/).

```
cd vikunja-app

cloudron build
cloudron install
```

## Testing

TBD

```
cd vikunja-app/test

npm install
PATH=$PATH:node_modules/.bin mocha --bail test.js
```

