#!/bin/bash

set -eu

mkdir -p /app/data/files \
        /app/data/logs \
        /run/vikunja

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data

# Creating config.yml and a secret APP_KEY if it does not exist
if [[ ! -f /app/data/config.yml ]]; then
  APP_KEY="$(dd if=/dev/urandom bs=32 count=1 | base64)"
  cp /app/code/api/config.yml.sample /app/data/config.yml
  yq e ".service.JWTSecret=\"${APP_KEY}\"" -i /app/data/config.yml
  yq e ".service.enableregistration=true" -i /app/data/config.yml
fi

# Replace config in config.yaml
echo "==> Configuring vikunja-api"

# Service config
yq e ".service.rootpath=\"/app/code/api/\"" -i /app/data/config.yml
yq e ".service.frontendurl=\"${CLOUDRON_APP_ORIGIN}/\"" -i /app/data/config.yml

# Database config - use postgres
yq e ".database.type=\"postgres\"" -i /app/data/config.yml
yq e ".database.user=\"${CLOUDRON_POSTGRESQL_USERNAME}\"" -i /app/data/config.yml
yq e ".database.password=\"${CLOUDRON_POSTGRESQL_PASSWORD}\"" -i /app/data/config.yml
yq e ".database.host=\"${CLOUDRON_POSTGRESQL_HOST}\"" -i /app/data/config.yml
yq e ".database.database=\"${CLOUDRON_POSTGRESQL_DATABASE}\"" -i /app/data/config.yml

# Cache config - use redis
yq e ".cache.enabled=true" -i /app/data/config.yml
yq e ".cache.type=\"redis\"" -i /app/data/config.yml

yq e ".redis.enabled=true" -i /app/data/config.yml
yq e ".redis.host=\"${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}\"" -i /app/data/config.yml
yq e ".redis.password=\"${CLOUDRON_REDIS_PASSWORD}\"" -i /app/data/config.yml

# Mail config - Can only be used encrypted
yq e ".mailer.enabled=true" -i /app/data/config.yml
yq e ".mailer.host=\"${CLOUDRON_MAIL_SMTP_SERVER}\"" -i /app/data/config.yml
yq e ".mailer.port=\"${CLOUDRON_MAIL_STARTTLS_PORT}\"" -i /app/data/config.yml
yq e ".mailer.username=\"${CLOUDRON_MAIL_SMTP_USERNAME}\"" -i /app/data/config.yml
yq e ".mailer.password=\"${CLOUDRON_MAIL_SMTP_PASSWORD}\"" -i /app/data/config.yml
yq e ".mailer.fromemail=\"${CLOUDRON_MAIL_FROM}\"" -i /app/data/config.yml
yq e ".mailer.skiptlsverify=true" -i /app/data/config.yml
yq e ".mailer.forcessl=false" -i /app/data/config.yml

# Log config
yq e ".log.path=\"/app/data/logs\"" -i /app/data/config.yml

# Files config
yq e ".files.basepath=\"/app/data/files/\"" -i /app/data/config.yml

echo "==> Starting vikunja frontend and API using supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Vikunja
