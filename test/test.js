#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */
/* global xit */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const username = process.env.USERNAME;
    const email = process.env.EMAIL;
    const password = process.env.PASSWORD;

    let browser, app;

    before(function (done) {
        if (!process.env.EMAIL) return done(new Error('EMAIL env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));

        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
        done();
    });

    after(function () {
        browser.quit();
    });

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function registerUser() {
        await browser.get(`https://${app.fqdn}/register`);
        await waitForElement(By.id('register-submit'));
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('email')).sendKeys(email);
        if (app.manifest.version === '1.0.3') {
            await browser.findElement(By.id('password1')).sendKeys(password);
            await browser.findElement(By.id('password2')).sendKeys(password);
        } else {
            await browser.findElement(By.id('password')).sendKeys(password);
        }
        await browser.findElement(By.id('registerform')).submit();
        await sleep(4000);
        await waitForElement(By.xpath(`//span[contains(text(), "${username}")]`));
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.id('username'));

        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.id('loginform')).submit();

        await sleep(2000);

        await waitForElement(By.xpath(`//span[contains(text(), "${username}")]`));
    }

    async function createLabel() {
        await browser.get(`https://${app.fqdn}/labels/new`);

        await sleep(2000);

        await waitForElement(By.id('labelTitle'));

        await browser.findElement(By.id('labelTitle')).sendKeys('CloudronLabel');
        await browser.findElement(By.id('labelTitle')).sendKeys(Key.ENTER);

        await sleep(3000);
    }

    async function checkLabel() {
        await browser.get(`https://${app.fqdn}/labels`);
        if (app.manifest.version === '1.0.3') {
            await waitForElement(By.xpath('//a[contains(text(), "CloudronLabel")]'));
        } else {
            await waitForElement(By.xpath('//button[text()="CloudronLabel"]'));
        }
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);

        await browser.sleep(2000);

        await waitForElement(By.xpath('//button[contains(@class, "username-dropdown-trigger")]'));
        await browser.findElement(By.xpath('//button[contains(@class, "username-dropdown-trigger")]')).click();

        await waitForElement(By.xpath('//button[./span[text()="Logout"]]'));
        await browser.findElement(By.xpath('//button[./span[text()="Logout"]]')).click();

        await waitForElement(By.xpath('//h2[text()="Login"]'));

        await browser.get('about:blank');
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can register new user', registerUser);
    it('can create label', createLabel);
    it('can logout', logout);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(5000);
    });

    it('can login', login);
    it('can check label', checkLabel);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('can check label', checkLabel);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can check label', checkLabel);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id io.vikunja.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can register new user', registerUser);
    it('can create label', createLabel);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can login', login);
    it('can check label', checkLabel);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
